#!/usr/bin/perl -w
#
#
#  rhautoimport
#
#  Copyright (C) 2009-2017 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhautoimport.
#
#  rhautoimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhautoimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhautoimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use Date::Calc;
use DateTime;
use DateTime::Format::ISO8601;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use URI::URL;
use HTML::Entities;
use RHRD::utils;

use lib '/usr/local/share/rhautoimport/';
use rhautoimport;

my $STAT_FILE = $ENV{'HOME'} . "/rhautoimport-fl.stat";
my $LAST_FILE = $ENV{'HOME'} . "/rhautoimport-fl.last";
my $RSS_URL = "https://cba.media/podcast/frequenz-leitwolf/feed";
$XML::Feed::MULTIPLE_ENCLOSURES=1;
my $RD_GROUP = "leitwolf";
my $PV_ID = '571';
my $TITLE="Frequenz Leitwolf";
my $LAST_RUN = 0;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

my $curweek = RHRD::utils::get_rd_week();

if($curweek == 1 || $curweek == 3) {
  if($#ARGV >= 0 && $ARGV[0] eq 'last') {
    print "!!!This is the last attempt, there won't be a retry on error!!!\n";
    $LAST_RUN = 1;
  }
}
rhautoimport::report_title_and_last($TITLE, $LAST_RUN);


my @allowed_dbs = rhautoimport::get_dropboxes($RD_GROUP);
if(!defined $allowed_dbs[0] && defined $allowed_dbs[1]) {
  print "$allowed_dbs[1]\n";
  exit 1;
}

if(scalar(@allowed_dbs) != 1) {
  print "found more or less than one Dropbox for this group?!\n";
  exit 1;
}
my $show_id = $allowed_dbs[0]->{'SHOWID'};
my $show_title = $allowed_dbs[0]->{'SHOWTITLE'};

my @today = Date::Calc::Today();
my @import_date = Date::Calc::Standard_to_Business(@today);
$import_date[2] = 6;
@import_date = Date::Calc::Business_to_Standard(@import_date);

my $dow = Date::Calc::Day_of_Week(@today);
if($curweek == 1 || $curweek == 3) {
  if($dow > 6) {
    @import_date = Date::Calc::Add_Delta_Days(@import_date, 14);
  }
} else {
  @import_date = Date::Calc::Add_Delta_Days(@import_date, 7);
}
my $last_broadcast = DateTime->from_epoch(epoch => 0);
my $last_broadcast_str = `cat $LAST_FILE`;
chomp($last_broadcast_str);
if(defined($last_broadcast_str) && $last_broadcast_str ne "") {
  $last_broadcast = DateTime::Format::ISO8601->parse_datetime($last_broadcast_str);
}

print "today: " . Date::Calc::Date_to_Text(@today) . " (Week: " . $curweek . ")\n";
print "day of next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";
print "Last broadcast was on: " . $last_broadcast . "\n\n";

my $id = sprintf("%04d-%02d-%02d", @import_date);

my $current_stat = `cat $STAT_FILE`;
my ($current_id, $current_file) = $current_stat =~ m/^(.*)\n(.*)/;
if($current_id eq $id) {
  print "Already downloaded current file\n";
  exit 42;
}

print "looking for file after $last_broadcast in RSS Feed\n";
print " -> $RSS_URL\n";

my ($result, $feed) = rhautoimport::fetch_parse_rss_cba($RSS_URL);
unless ($result) {
  print "Error fetching feed: $feed\n";
  exit 1;
}

my $shows_left = -1;
my $uri = "";
my $file = "";
my $sum_title = "";
my $sum_text = "";
my $this_broadcast = $last_broadcast;

my $i = 0;
for my $entry ($feed->entries) {
  $i++;
  next unless $entry->enclosure;

  my $issued = $entry->issued;
  next unless (defined $issued);
  my $idt = DateTime::Format::ISO8601->parse_datetime($issued);

  my $diff = $idt->subtract_datetime_absolute($last_broadcast);
  last if($diff->is_zero || $diff->is_negative);

  $shows_left++;
  $sum_title = decode_entities($entry->title);
  $sum_text = decode_entities($entry->content->body);
  my $j = 0;
  for my $enclosure($entry->enclosure) {
    $j++;
    if($enclosure->type eq "audio/mpeg" || $enclosure->type eq "audio/ogg") {
      my $orig_uri = $enclosure->url;
      if($orig_uri =~ /^(.*)_cut(\.[^\.]+)$/) {
        $orig_uri = $1 . $2;
      }

      $uri = new URI::URL($orig_uri);
      my @path = $uri->path_components;
      $file = $path[-1];

      if(!rhautoimport::check_file_extension($file)) {
        print "\n\nThe extension of the matching file '". $file . "' seems to be wrong - manual import necessary!!!\n";
        print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
        exit 1;
      }

      $this_broadcast = $idt;
      last;
    }
  }
}
if($uri eq "") {
  print "No Entry for $last_broadcast (there are no new shows available) - ";
  if($LAST_RUN) {
    print "giving up, manual import necessary!!!\n";
    print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
  } else {
    print "will retry later\n";
  }
  exit 1;
}

my $exit_code = 0;
if($shows_left <= 0) {
  print "\n ****** There are *no* shows remaining! ******\n\n";
  $exit_code = 23
} elsif($shows_left < 4) {
  print "\n ** There are only $shows_left shows remaining! **\n\n";
  $exit_code = 23;
} else {
  print "\nThere are $shows_left shows remaining\n\n";
}

print "summary:\n" . $sum_title . "\n\n" . $sum_text . "\n";
print "\n\nwill import '$uri' to show $show_id, $show_title\n\n";

my ($ret, $log) = rhautoimport::import_uri($show_id, $uri->as_string);
if($ret) {
  print "\nImport Error:\n\n";
  print $log;
  print "\n\nNot adding PV note!!";
  exit 1;
}
print "\nImport Success:\n\n";
print $log;
print "\n";
($ret, $log) = rhautoimport::pv_add_note($sum_title, $sum_text, $PV_ID, $id, "1");
print $log;
if($ret) {
  print "\nIgnoring failed note import - manual intervention necessary!\n";
  $exit_code = 23;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs "$id\n$file";
close($fhs);

unlink($LAST_FILE);
open($fhs, '>', $LAST_FILE);
print $fhs $this_broadcast->iso8601();
close($fhs);

exit $exit_code;
