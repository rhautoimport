#!/usr/bin/perl -w
#
#
#  rhautoimport
#
#  Copyright (C) 2009-2017 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhautoimport.
#
#  rhautoimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhautoimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhautoimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use Date::Calc;
use DateTime::Format::Strptime;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use URI::URL;
use HTML::Entities;
use RHRD::utils;

use lib '/usr/local/share/rhautoimport/';
use rhautoimport;

my $STAT_FILE = $ENV{'HOME'} . "/rhautoimport-mz.stat";
my $RSS_URL = "https://cba.media/podcast/medienzirkus/feed";
$XML::Feed::MULTIPLE_ENCLOSURES=1;
my $RD_GROUP = "medienzi";
my $PV_ID = '913';
my $TITLE = "Medienzirkus";
my $LAST_RUN = 0;
my $upload_delay_days = 0;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

my $curweek = RHRD::utils::get_rd_week();

if($curweek == 4) {
  if($#ARGV >= 0 && $ARGV[0] eq 'last') {
    print "!!!This is the last attempt, there won't be a retry on error!!!\n";
    $LAST_RUN = 1;
  }
}
rhautoimport::report_title_and_last($TITLE, $LAST_RUN);


my @today = Date::Calc::Today();
print "today: " . Date::Calc::Date_to_Text(@today) . " (Week: " . $curweek . ")\n";

my @import_date = Date::Calc::Standard_to_Business(@today);
$import_date[2] = 5;
@import_date = Date::Calc::Business_to_Standard(@import_date);

my $dow = Date::Calc::Day_of_Week(@today);
if($curweek == 4) {
  if($dow >= 5) {
    @import_date = Date::Calc::Add_Delta_Days(@import_date, 28);
  }
} elsif($curweek == 3) {
  @import_date = Date::Calc::Add_Delta_Days(@import_date, 7);
} elsif($curweek == 2) {
  @import_date = Date::Calc::Add_Delta_Days(@import_date, 14);
} else {
  @import_date = Date::Calc::Add_Delta_Days(@import_date, 21);
}
print "day of next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";


my @allowed_dbs = rhautoimport::get_dropboxes($RD_GROUP);
if(!defined $allowed_dbs[0] && defined $allowed_dbs[1]) {
  print "$allowed_dbs[1]\n";
  exit 1;
}

if(scalar(@allowed_dbs) != 1) {
  print "found more or less than one Dropbox for this group?!\n";
  exit 1;
}
my $show_id = $allowed_dbs[0]->{'SHOWID'};
my $show_title = $allowed_dbs[0]->{'SHOWTITLE'};

my @broadcast_date = Date::Calc::Nth_Weekday_of_Month_Year($import_date[0], $import_date[1], 5, 1);
if(Date::Calc::Delta_Days(@broadcast_date, @import_date) <= 0) {
  my @tmp_date = Date::Calc::Add_Delta_YM(@broadcast_date, 0, -1);
  @broadcast_date = Date::Calc::Nth_Weekday_of_Month_Year($tmp_date[0], $tmp_date[1], 5, 1);
}
print "day of latest original broadcast before next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@broadcast_date) . "\n";

if(Date::Calc::Delta_Days(@broadcast_date, @today) <= $upload_delay_days) {
  print "File won't be available by now!\n";
  exit 42;
}

my $id = sprintf("%04d-%02d-%02d", @import_date);
my $bd = sprintf("%04d-%02d-%02d", @broadcast_date);

print "looking for files with cba:broadcastDate $bd in RSS Feed (will be using cba:productionDate if cba:broadcastDate is not set)\n";
print " -> $RSS_URL\n";

my ($result, $feed) = rhautoimport::fetch_parse_rss_cba($RSS_URL);
unless ($result) {
  print "Error fetching feed: $feed\n";
  exit 1;
}

my $uri = "";
my $file = "";
my $sum_title = "";
my $sum_text = "";

my $i = 0;
for my $entry ($feed->entries) {
  $i++;
  next unless $entry->enclosure;

  my $entry_bdt = $entry->{'entry'}{'https://cba.media/help#feeds'}{'broadcastDate'};
  my $format_string = "%a, %d %b %Y %H:%M:%S";
  unless (defined $entry_bdt) {
    $entry_bdt = $entry->{'entry'}{'https://cba.media/help#feeds'}{'productionDate'};
    $format_string = "%a, %d %b %Y";
  }
  next unless (defined $entry_bdt);
  my $bdt = DateTime::Format::Strptime::strptime($format_string, $entry_bdt);

  next unless($broadcast_date[0] == $bdt->year && $broadcast_date[1] == $bdt->month && $broadcast_date[2] == $bdt->day);

  my $j = 0;
  for my $enclosure($entry->enclosure) {
    $j++;
    if($enclosure->type eq "audio/mpeg" || $enclosure->type eq "audio/ogg") {
      print "$i/$j: (" . $enclosure->type . ", " . $enclosure->length . ") "  . $enclosure->url . "\n";
      my $orig_uri = $enclosure->url;
      if($orig_uri =~ /^(.*)_cut(\.[^\.]+)$/) {
        $orig_uri = $1 . $2;
      }

      $uri = new URI::URL($orig_uri);
      my @path = $uri->path_components;
      $file = $path[-1];

      my $current_stat = `cat $STAT_FILE`;
      my ($current_id, $current_file) = $current_stat =~ m/^(.*)\n(.*)/;
      if($current_id eq $id  &&  $current_file eq $file) {
        print "Already downloaded file of today\n";
        exit 42;
      }
      if(!rhautoimport::check_file_extension($file)) {
        print "\n\nThe extension of the matching file '". $file . "' seems to be wrong - manual import necessary!!!\n";
        print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
        exit 1;
      }

      $sum_title = decode_entities($entry->title);
      $sum_text = decode_entities($entry->content->body);
      print "summary:\n" . $sum_title . "\n\n" . $sum_text . "\n";
      last;
    }
  }
  last if $uri ne "";
}
if($uri eq "") {
  print "No Entry found from $bd - ";
  if($LAST_RUN) {
    print "giving up, manual import necessary!!!\n";
    print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
  } else {
    print "will retry later\n";
  }
  exit 1;
}

print "\n\nwill import '$uri' to show $show_id, $show_title\n\n";

my ($ret, $log) = rhautoimport::import_uri($show_id, $uri->as_string);
if($ret) {
  print "\nImport Error:\n\n";
  print $log;
  print "\n\nNot adding PV note!!";
  exit 1;
}
print "\nImport Success:\n\n";
print $log;
print "\n";
my $exit_code = 0;
($ret, $log) = rhautoimport::pv_add_note($sum_title, $sum_text, $PV_ID, $id, "1");
print $log;
if($ret) {
  print "\nIgnoring failed note import - manual intervention necessary!\n";
  $exit_code = 23;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs "$id\n$file";
close($fhs);

exit $exit_code;
