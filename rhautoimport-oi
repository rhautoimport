#!/usr/bin/perl -w
#
#
#  rhautoimport
#
#  Copyright (C) 2009-2017 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhautoimport.
#
#  rhautoimport is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhautoimport is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with rhautoimport. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use Date::Calc;
use XML::Feed;
use XML::Feed::Entry;
use XML::Feed::Content;
use XML::Feed::Enclosure;
use URI::URL;
use HTML::Entities;
use RHRD::utils;

use lib '/usr/local/share/rhautoimport/';
use rhautoimport;

my $STAT_FILE = $ENV{'HOME'} . "/rhautoimport-oi.stat";
my $LAST_FILE = $ENV{'HOME'} . "/rhautoimport-oi.last";
my $RSS_URL = "https://www.npla.de/feed/podcast/?podcast_series=onda-info";
my $RD_GROUP = "ondainfo";
my $PV_ID = '220';
my $TITLE="Onda-Info";
my $LAST_RUN = 0;

binmode(STDIN, ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

my $curweek = RHRD::utils::get_rd_week();
if($curweek == 1 || $curweek == 3) {
  print "please don't run this script in week 1 or 3!\n";
  rhautoimport::report_title_and_last($TITLE, 0);
  exit 42;
}

if($#ARGV >= 0 && $ARGV[0] eq 'last') {
  print "!!!This is the last attempt, there won't be a retry on error!!!\n";
  $LAST_RUN = 1;
}
rhautoimport::report_title_and_last($TITLE, $LAST_RUN);


my @allowed_dbs = rhautoimport::get_dropboxes($RD_GROUP);
if(!defined $allowed_dbs[0] && defined $allowed_dbs[1]) {
  print "$allowed_dbs[1]\n";
  exit 1;
}

my $idx = 0;
my $idx_reb = 1;
if(scalar(@allowed_dbs) != 2) {
  print "found more or less than 2 Dropboxes for this group?!\n";
  exit 1;
}
my $show_id = $allowed_dbs[$idx]->{'SHOWID'};
my $show_title = $allowed_dbs[$idx]->{'SHOWTITLE'};
my $show_id_reb = $allowed_dbs[$idx_reb]->{'SHOWID'};
my $show_title_reb = $allowed_dbs[$idx_reb]->{'SHOWTITLE'};


my @today = Date::Calc::Today();
my @import_date = Date::Calc::Add_Delta_Days(@today, 7);
@import_date = Date::Calc::Standard_to_Business(@import_date);
$import_date[2] = 1;
@import_date = Date::Calc::Business_to_Standard(@import_date);
my @import_date_reb = Date::Calc::Add_Delta_Days(@import_date, 4);

my $broadcast_num = `cat $LAST_FILE`;
$broadcast_num += 1;

print "today: " . Date::Calc::Date_to_Text(@today) . " (Week: " . $curweek . ")\n";
print "day of next Radio Helsinki broadcast: " . Date::Calc::Date_to_Text(@import_date) . "\n";
print "day of next Radio Helsinki rebroadcast: " . Date::Calc::Date_to_Text(@import_date_reb) . "\n";
print "Number of next original broadcast: " . $broadcast_num . "\n\n";

my $id = sprintf("%04d-%02d-%02d", @import_date);
my $bdnumexp = sprintf('onda-info\s*%d', $broadcast_num);

my $current_stat = `cat $STAT_FILE`;
my ($current_id, $current_file) = $current_stat =~ m/^(.*)\n(.*)/;
if($current_id eq $id) {
  print "Already downloaded current file\n";
  exit 42;
}

print "looking for title like '$bdnumexp' in RSS Feed\n";
print " -> $RSS_URL\n";

my ($result, $feed) = rhautoimport::fetch_parse_rss($RSS_URL);
unless ($result) {
  print "Error fetching feed: $feed\n";
  exit 1;
}

my $uri = "";
my $file = "";
my $sum_title = "";
my $sum_text = "";

for my $entry ($feed->entries) {
  if($entry->enclosure) {
    print $entry->enclosure->url . "\n";

    $sum_title = decode_entities($entry->title);
    next unless $sum_title =~ /$bdnumexp/;

    $uri = new URI::URL($entry->enclosure->url);
    my @path = $uri->path_components;
    $file = $path[-1];

    if(!rhautoimport::check_file_extension($file)) {
      print "\n\nThe extension of the matching file '". $file . "' seems to be wrong - manual import necessary!!!\n";
      print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
      exit 1;
    }

    $sum_title = decode_entities($entry->title);
    $sum_text = decode_entities($entry->content->body);
    print "summary:\n" . $sum_title . "\n\n" . $sum_text . "\n";
    last;
  }
}
if($uri eq "") {
  print "No Entry found with #" . $broadcast_num . " - ";
  if($LAST_RUN) {
    print "giving up, manual import necessary!!!\n";
    print "\n\n --> https://import.helsinki.at/shows/$show_id\n";
  } else {
    print "will retry later\n";
  }
  exit 1;
}

my $exit_code = 0;
print "\n\nwill import '$uri' to show $show_id, $show_title\n";
my ($ret, $log, $keptfile_uri) = rhautoimport::import_uri($show_id, $uri->as_string, "keep");
if($ret == 0) {
  print "\nImport Success:\n\n";
  print $log;
  print "\n";
  ($ret, $log) = rhautoimport::pv_add_note($sum_title, $sum_text, $PV_ID, sprintf("%04d-%02d-%02d", @import_date), "1");
  print $log;
  if($ret) {
    print "\nIgnoring failed note import - manual intervention necessary!\n";
    $exit_code = 23;
  }

  print "\n";

  if(!defined($keptfile_uri)) {
    print "rhimportd didn't provide a clue where to find the kept file... will import '$uri' to rebroadcast $show_id_reb, $show_title_reb\n";
    ($ret, $log) = rhautoimport::import_uri($show_id_reb, $uri->as_string);
  } else {
    print "re-using kept file '$keptfile_uri' to import rebroadcast $show_id_reb, $show_title_reb\n";
    ($ret, $log) = rhautoimport::import_uri($show_id_reb, $keptfile_uri, "delete");
  }

  if($ret == 0) {
    print "\nImport Success:\n\n";
    print $log;
    print "\n";
    ($ret, $log) = rhautoimport::pv_add_note($sum_title, $sum_text, $PV_ID, sprintf("%04d-%02d-%02d", @import_date_reb), "2");
    print $log;
    if($ret) {
      print "\nIgnoring failed note import - manual intervention necessary!\n";
      $exit_code = 23;
    }

  } else {
    print "\nImport Error:\n\n";
    print $log;
    print "\n\nNot adding PV note!!";
    print "\n\nSince the import for the regular broadcast went through we will not retry this import!!!";
    print "\nYou need to manually import the rebroadcast.";
    print "\n\n --> https://import.helsinki.at/shows/$show_id_reb\n";
    $exit_code = 23;
  }
} else {
  print "\nImport Error:\n\n";
  print $log;
  print "\n\nNot adding PV note!!";
  exit 1;
}

unlink($STAT_FILE);
open(my $fhs, '>', $STAT_FILE);
print $fhs "$id\n$file";
close($fhs);

unlink($LAST_FILE);
open($fhs, '>', $LAST_FILE);
print $fhs "$broadcast_num";
close($fhs);

exit $exit_code;
