##
##  rhautoimport
##
##  Copyright (C) 2009-2017 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhautoimport.
##
##  rhautoimport is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhautoimport is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhautoimport. If not, see <http://www.gnu.org/licenses/>.
##

ifneq ($(MAKECMDGOALS),distclean)
include include.mk
endif

.PHONY: clean distclean

EXECUTABLE := rhautoimport
IMPORTER := ar bo dn ek eu fv gd kk lt mc oi rs ra sol
EXECUTABLES := $(IMPORTER:%=$(EXECUTABLE)-%)

all: $(EXECUTABLE)

distclean: clean
	find . -name "*.\~*" -exec rm -rf {} \;
	rm -f include.mk

clean:

INSTALL_TARGETS := install-bin install-share install-etc
REMOVE_TARGETS := remove-bin remove-share
PURGE_TARGETS := remove remove-etc

install: $(INSTALL_TARGETS)

install-bin:
	$(INSTALL) -d $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE) $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLES) $(DESTDIR)$(BINDIR)
	@for exe in $(EXECUTABLES); do \
     sed s#/usr/local/share/rhautoimport/#$(SHAREDIR)/$(EXECUTABLE)/#g -i $(DESTDIR)$(BINDIR)/$$exe ; \
  done

install-share:
	$(INSTALL) -d $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)
	$(INSTALL) -m 644 rhautoimport.pm $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)

install-etc:
	$(INSTALL) -d $(DESTDIR)$(ETCDIR)/$(EXECUTABLE)
	$(INSTALL) -d $(DESTDIR)$(ETCDIR)/cron.d/
	$(INSTALL) -m 644 cron $(DESTDIR)$(ETCDIR)/cron.d/$(EXECUTABLE)


uninstall: remove


remove: $(REMOVE_TARGETS)

remove-bin:
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)
	@for exe in $(EXECUTABLES); do \
		rm -f $(DESTDIR)$(BINDIR)/$$exe ; \
	done

remove-share:
	rm -rf $(DESTDIR)$(SHAREDIR)/$(EXECUTABLE)

remove-etc:
	rm -rf $(DESTDIR)$(ETCDIR)


purge: $(PURGE_TARGETS)
